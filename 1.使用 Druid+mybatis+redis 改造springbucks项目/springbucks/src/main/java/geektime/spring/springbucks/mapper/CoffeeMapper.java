package geektime.spring.springbucks.mapper;

import java.math.BigDecimal;
import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import geektime.spring.springbucks.model.Coffee;

@Mapper
public interface CoffeeMapper {

	@Insert("insert into t_coffee (name, price, create_time, update_time)" + "values (#{name}, #{price}, now(), now())")
	@Options(useGeneratedKeys = true)
	int insertCoffee(Coffee coffee);

	@Select("select * from t_coffee where id = #{id}")
	@Results({ @Result(id = true, column = "id", property = "id"),
			   @Result(column = "create_time", property = "createTime") })
	Coffee findById(@Param("id") Long id);

	@Select("select * from t_coffee")
	@Results({ @Result(column = "name", property = "name"), @Result(column = "create_time", property = "createTime") })
	List<Coffee> findAllCoffees();
	
	@Delete("delete from t_coffee where id = #{id}")
	int deleteCoffee(@Param("id") Long id);
	
	@Update("update t_coffee set name=#{name},price=#{price}, update_time=now() where id=#{id}")
	int updateCoffee(@Param("id") Long id,@Param("name") String name,@Param("price") BigDecimal price);
	
	
	 @Select("select * from t_coffee order by id")
	 List<Coffee> findAllWithParam(@Param("pageNum") int pageNum,
	                               @Param("pageSize") int pageSize);
}
