package geektime.spring.springbucks.service;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.stereotype.Repository;

import geektime.spring.springbucks.model.Coffee;

@Repository
public interface CoffeeService {
  
	/**
	 * 查询所有咖啡列表
	 * @return 咖啡列表
	 */
    public List<Coffee> findAllCoffees();
    
    /**
     * 根据id批量查询咖啡列表
     * @param list 咖啡id列表
     * @return 咖啡列表
     */
    public List<Coffee> findCoffeesByIds(List<Long> list);
    
    /**
     * 刷新缓存列表
     */
    public void reloadCoffee();
    
    /**
     * 根据id查询咖啡
     * @param id 咖啡id
     * @return 咖啡实体
     */
    public Coffee findById(Long id);
    
    /**
     * 插入新的咖啡信息
     * @param name 咖啡名称
     * @param price 咖啡价格
     * @return
     */
    public int insertCoffee(String name,BigDecimal price);
    
    /**
     * 根据ID删除咖啡
     * @param id 咖啡id
     * @return 返回影响记录数
     */
    public int deleteCoffee(Long id);
    
    /**
     * 根据咖啡ID更新信息
     * @param id 咖啡ID
     * @param name 咖啡名称
     * @param price 咖啡价格
     * @return 返回影响记录数
     */
    public int updateCoffee(Long id,String name,BigDecimal price);
    
    /**
     * 分页查询
     * @param pageNum 页数
     * @param pageSize 每页记录数
     * @return 返回咖啡列表
     */
    public List<Coffee> findAllWithParam(int pageNum,int pageSize);
    
    
}
