package geektime.spring.springbucks;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.github.pagehelper.PageInfo;

import geektime.spring.springbucks.model.Coffee;
import geektime.spring.springbucks.service.CoffeeService;
import lombok.extern.slf4j.Slf4j;

@SpringBootApplication
@Slf4j
@MapperScan("geektime.spring.springbucks.mapper")
@EnableTransactionManagement //开启事务支持
@EnableCaching(proxyTargetClass = true)////开启支持缓存的注解 并基于类进行代理
public class SpringBucksApplication implements ApplicationRunner {

	@Autowired
	private DataSource dataSource;
	
	@Autowired
	private CoffeeService coffeeService;

	public static void main(String[] args) {
		SpringApplication.run(SpringBucksApplication.class, args);
	}

	@Override
	public void run(ApplicationArguments args) throws Exception {
		
		showDataSource();
		
		//Spring 缓存
		log.info("All Coffee: {}", coffeeService.findAllCoffees().size());
		for (int i = 0; i < 10; i++) {
			log.info("Reading from Spring cache.");
			coffeeService.findAllCoffees();
		}
		coffeeService.reloadCoffee();
		log.info("Reading after refresh.");
		coffeeService.findAllCoffees().forEach(c -> log.info("Coffee {}", c.getName()));
		
		// redis 缓存
		log.info("All Coffee: {}", coffeeService.findAllCoffees().size());
		for (int i = 0; i < 10; i++) {
			log.info("Reading from redis cache.");
			coffeeService.findAllCoffees();
		}
		Thread.sleep(5_000);//睡5秒
		log.info("Reading after refresh.");
		coffeeService.findAllCoffees().forEach(c -> log.info("Coffee {}", c.getName()));
		
		//插入
		int count = coffeeService.insertCoffee("bonc", BigDecimal.TEN);
		log.info("Save {} Coffee",count );
		log.info("After Save Coffee: {}", coffeeService.findAllCoffees());
		
		//删除
		count  = coffeeService.deleteCoffee(6l);
		log.info("Delete {} Coffee",count );
		log.info("After Delete Coffee: {}", coffeeService.findAllCoffees());
		
		//更新
		count  = coffeeService.updateCoffee(2L, "NanNing", BigDecimal.ZERO);
		log.info("Update {} Coffee",count );
		log.info("After Update Coffee: {}", coffeeService.findAllCoffees());
		
		//单记录查询
		Coffee coffee = coffeeService.findById(1l);
		log.info("one coffee info {} ",coffee.toString() );
		
		//分页查询
		List<Coffee> list = coffeeService.findAllWithParam(2, 3);
		PageInfo page = new PageInfo(list);
		log.info("Coffee PageInfo: {}", page);
		
		//根据id批量查询，并加入缓存
		List<Long> idList = new ArrayList<Long>();
		idList.add(1l);
		idList.add(2l);
		idList.add(3l);
		idList.add(4l);
		idList.add(5l);
		
		List<Coffee> coffeeList = coffeeService.findCoffeesByIds(idList);
		coffeeList.forEach(c -> log.info("Coffee {}", c.getName()));
	}
	
	/**
	 * 显示使用的数据源类型
	 */
	private void showDataSource() {
		log.info("DataSource Info {}",dataSource.toString());
	}
}
